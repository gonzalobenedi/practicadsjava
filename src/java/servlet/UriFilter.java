/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author gonza
 */
public class UriFilter implements Filter {
    private static final Logger LOG = Logger.getLogger(UriFilter.class.getName());
    
    private static final boolean debug = true;

    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured. 
    private FilterConfig filterConfig = null;
    
    public UriFilter() {
    }    
    
    private void doBeforeProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
        if (debug) {
            log("UriFilter:DoBeforeProcessing");
        }

        // Write code here to process the request and/or response before
        // the rest of the filter chain is invoked.
        // For example, a logging filter might log items on the request object,
        // such as the parameters.
        /*
	for (Enumeration en = request.getParameterNames(); en.hasMoreElements(); ) {
	    String name = (String)en.nextElement();
	    String values[] = request.getParameterValues(name);
	    int n = values.length;
	    StringBuffer buf = new StringBuffer();
	    buf.append(name);
	    buf.append("=");
	    for(int i=0; i < n; i++) {
	        buf.append(values[i]);
	        if (i < n-1)
	            buf.append(",");
	    }
	    log(buf.toString());
	}
         */
    }    
    
    private void doAfterProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
        if (debug) {
            log("UriFilter:DoAfterProcessing");
        }

        // Write code here to process the request and/or response after
        // the rest of the filter chain is invoked.
        // For example, a logging filter might log the attributes on the
        // request object after the request has been processed. 
        /*
	for (Enumeration en = request.getAttributeNames(); en.hasMoreElements(); ) {
	    String name = (String)en.nextElement();
	    Object value = request.getAttribute(name);
	    log("attribute: " + name + "=" + value.toString());

	}
         */
        // For example, a filter might append something to the response.
        /*
	PrintWriter respOut = new PrintWriter(response.getWriter());
	respOut.println("<P><B>This has been appended by an intrusive filter.</B>");
         */
    }

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain)
            throws IOException, ServletException {
        
        //casting de ServletRequest a HttpServletRequest para poder leer la URI
        HttpServletRequest request = (HttpServletRequest) req;
        //Tomamos la URI y le quitamos el contexto
        String requestURI = request.getRequestURI();              
        requestURI = requestURI.substring(request.getContextPath().length()+1);
        LOG.info(requestURI);

        if (requestURI.endsWith(".css") || requestURI.endsWith(".js")) {
            chain.doFilter(req, res);            
        } else {
//            chain.doFilter(req, res);            
//            req.setAttribute("algo", requestURI );
//                    request.getRequestDispatcher("/userIndex").forward(req, res);
            route(req, res, requestURI);
        }
    }
    
    private void route(ServletRequest req, ServletResponse res, String uri) 
            throws ServletException, IOException {
        
            String[] uriParams = uri.split("/");
            List<String> args;  
            args = new LinkedList<>(Arrays.asList(uriParams));
           
            
            
            LOG.log(Level.INFO, "{0}", uri);
            LOG.log(Level.INFO, "{0}", uriParams.length);
            
            String controller, method;
            LOG.log(Level.INFO, "A por el controlador: {0} elementos", args.size());
            controller = args.remove(0); 
            LOG.info(controller);
            if (args.isEmpty()) {
                method = "";
            } else {
                method = args.remove(0); 
            }

            
            String servletName;
            switch (controller.toLowerCase()) {
                case "": 
                    servletName = "/WEB-INF/view/index.jsp";
                    req.getRequestDispatcher(servletName).forward(req, res);
                case "client":
                    routeClient(req, res, method, args);
                case "login":
                    routeLogin(req, res, method, args);
                case "help":
                    servletName = "/WEB-INF/view/help.jsp";
                    req.getRequestDispatcher(servletName).forward(req, res);
                default:
                    servletName = "/WEB-INF/view/index.jsp";
                    req.getRequestDispatcher(servletName).forward(req, res);
            }
    }
           
    public void routeClient(ServletRequest req, ServletResponse res, String method, List<String> args) 
            throws ServletException, IOException {
        String servletName = "";
        switch (method.toLowerCase()) {
            case "":
            case "index":
                servletName = "/ClientIndex";
                req.setAttribute("param", args);
                break;
            case "new":
                    servletName = "/WEB-INF/view/client/clientNew.jsp";
                break;
            case "insert":
                servletName = "/ClientInsert";
                break;
            case "edit":
                servletName = "/ClientEdit";
                break;
            case "update":
                servletName = "/ClientUpdate";
                break;
            case "search":
                servletName = "/WEB-INF/view/client/clientSearch.jsp";
                break;
            case "find":
                servletName = "/ClientSearch";
                break;
        }
        LOG.log(Level.INFO, "Forward a: {0}", servletName);
        req.getRequestDispatcher(servletName).forward(req, res);        
    }
    
    public void routeLogin(ServletRequest req, ServletResponse res, String method, List<String> args) 
            throws ServletException, IOException {
        String servletName = "";
        switch (method.toLowerCase()) {
            case "":
            case "index":
                servletName = "/WEB-INF/view/index.jsp";
                break;
            case "in":
                servletName = "/LoginIn";
                break;
            case "out":
                servletName = "/LoginOut";
                break;
        }
        LOG.log(Level.INFO, "Forward a: {0}", servletName);
        req.getRequestDispatcher(servletName).forward(req, res);        
    }

    /**
     * Return the filter configuration object for this filter.
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    public void destroy() {        
    }

    /**
     * Init method for this filter
     */
    public void init(FilterConfig filterConfig) {        
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            if (debug) {                
                log("UriFilter:Initializing filter");
            }
        }
    }

    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("UriFilter()");
        }
        StringBuffer sb = new StringBuffer("UriFilter(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }
    
    private void sendProcessingError(Throwable t, ServletResponse response) {
        String stackTrace = getStackTrace(t);        
        
        if (stackTrace != null && !stackTrace.equals("")) {
            try {
                response.setContentType("text/html");
                PrintStream ps = new PrintStream(response.getOutputStream());
                PrintWriter pw = new PrintWriter(ps);                
                pw.print("<html>\n<head>\n<title>Error</title>\n</head>\n<body>\n"); //NOI18N

                // PENDING! Localize this for next official release
                pw.print("<h1>The resource did not process correctly</h1>\n<pre>\n");                
                pw.print(stackTrace);                
                pw.print("</pre></body>\n</html>"); //NOI18N
                pw.close();
                ps.close();
                response.getOutputStream().close();
            } catch (Exception ex) {
            }
        } else {
            try {
                PrintStream ps = new PrintStream(response.getOutputStream());
                t.printStackTrace(ps);
                ps.close();
                response.getOutputStream().close();
            } catch (Exception ex) {
            }
        }
    }
    
    public static String getStackTrace(Throwable t) {
        String stackTrace = null;
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            pw.close();
            sw.close();
            stackTrace = sw.getBuffer().toString();
        } catch (Exception ex) {
        }
        return stackTrace;
    }
    
    public void log(String msg) {
        filterConfig.getServletContext().log(msg);        
    }
    
}
