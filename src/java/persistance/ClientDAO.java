/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistance;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Client;

/**
 *
 * @author gonza
 */
public class ClientDAO {
    private Connection connection = null;
    private PreparedStatement stmt = null;
    private static final String CLASS_DRIVER = "com.mysql.jdbc.Driver";
    private static final String URL="jdbc:mysql://londonapp.cufhmzckqhkn.eu-west-1.rds.amazonaws.com:3306/mvc";//conexión con bbdd AWS
//    private static final String URL = "jdbc:mysql://10.2.25.47/mvc"; //conexión desde Windows a la bbdd de Ubuntu
    private static final Logger LOG = Logger.getLogger(ClientDAO.class.getName());
    private ResultSet rs;

    //metodo connect
    public void connect() {
        try {
            Class.forName(ClientDAO.CLASS_DRIVER);
            connection = DriverManager.getConnection(ClientDAO.URL, "gonzalonbc", "rickety450lapse"); //conexion AWS
            //connection = DriverManager.getConnection(ClientDAO.URL, "usuario", "usuario");//conexion localhost Ubuntu
            if (connection != null) {
                LOG.info("Conexión establecida!");
            } else {
                LOG.severe("Fallo de conexión!");
            }
        } catch (ClassNotFoundException ex) {
            LOG.log(Level.SEVERE,
                    "No se pudo cargar el driver de la base de datos", ex);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE,
                    "No se pudo obtener la conexión a la base de datos", ex);
        }
    }
//metodo disconnect

    public void disconnect() {
        try {
            connection.close();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }
    
//    Volcar Todos
    public ArrayList<Client> getAll() {
        ArrayList<Client> clients = null;        
        try {
            stmt = connection.prepareStatement("select * from client");
            rs = stmt.executeQuery();
            clients = new ArrayList();
            int i = 0;
            while (rs.next()) {
                i++;
                Client client = new Client();
                client.setId(rs.getInt("id"));
                client.setName(rs.getString("name"));
                client.setAddress(rs.getString("address"));
                client.setPhone(rs.getString("phone"));
                client.setCredit(rs.getDouble("credit"));
                clients.add(client);
                LOG.info("Registro fila: " + i);
            }
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return clients;
    }
    
//    Volcar por ID
    public Client getUser(int id) throws SQLException{
        try{
            stmt = connection.prepareStatement("select * from client where id=?");
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            Client client = new Client();
            if(rs.next()){
                client.setId(rs.getInt("id"));
                client.setName(rs.getString("name"));
                client.setAddress(rs.getString("address"));
                client.setPhone(rs.getString("phone"));
                client.setCredit(rs.getInt("id"));
            }
            return client;
        }catch(SQLException e){
            LOG.log(Level.SEVERE, null, e);
        }
        return null;
    }
    
//    Insertar
    public int insert(Client client) {
        try {
            stmt = connection.prepareStatement("insert into client (name, address, phone, credit) values(?, ?, ?, ?)");
            stmt.setString(1, client.getName());
            stmt.setString(2, client.getAddress());
            stmt.setString(3, client.getPhone());
            stmt.setDouble(4, client.getCredit());

            return stmt.executeUpdate();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
//    Update
    public int update(Client client){
        try {
            stmt = connection.prepareStatement("update user set name=?, address=?, phone=?, credit=? where id=?");
            stmt.setString(1, client.getName());
            stmt.setString(2, client.getAddress());
            stmt.setString(3, client.getPhone());
            stmt.setDouble(4, client.getCredit());
            stmt.setInt(5, client.getId());
            return stmt.executeUpdate();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return 0;
        }
    }

//    Delete
    public int delete(int id){
        try{
            stmt = connection.prepareStatement("delete from client where id=?");
            stmt.setInt(1, id);
            return stmt.executeUpdate();
        }catch(SQLException e){
            LOG.log(Level.SEVERE, null, e);
            return 0;
        }
        
    }
    
    public ArrayList<Client> search(String chain) {
        ArrayList<Client> clients = null;        
        try {
            stmt = connection.prepareStatement("select * from client where name=?");
            stmt.setString(1, chain);
            rs = stmt.executeQuery();
            clients = new ArrayList();
            int i = 0;
            while (rs.next()) {
                i++;
                Client client = new Client();
                client.setId(rs.getInt("id"));
                client.setName(rs.getString("name"));
                client.setAddress(rs.getString("address"));
                client.setPhone(rs.getString("phone"));
                client.setCredit(rs.getDouble("credit"));
                clients.add(client);
                LOG.info("Registro fila: " + i);
            }
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return clients;
    }
    
}
