<%-- 
    Document   : help
    Created on : 11-dic-2016, 12:23:21
    Author     : gonza
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/default.css">
    <title>Práctica Java</title>
</head>
<body>
<div id="header">
<div id="title">Práctica Java 1ª Evaluación</div>
<div>
    <a href="<%= request.getContextPath() %>/client">Cliente</a>
    <a href="<%= request.getContextPath() %>/help">Ayuda</a>
</div>
</div>

    
    <div id="content">
    <% 
    try{
        if(!request.getSession().getAttribute("username").equals("")){
    %>
            <p>Hola <%=request.getSession().getAttribute("username")%> || <a href="<%= request.getContextPath() %>/login/out">Logout</a></p>
    <%
        }else{
    %>
            <p>Hola Invitado || <a href="<%= request.getContextPath() %>/login">Login</a></p>
    <%
        }
    }catch(Exception e){
    %>
    <p>Hola Invitado || <a href="<%= request.getContextPath() %>/login">Login</a></p>
    <%
    }
    %>
    <h2>Funcionalidades</h2>
    <ul>
      <li><p>/login/index Debe mostrar una pantalla con formulario donde se registre el nombre del usuario.
        El formulario tendrá un action /login/in, no es precisa contraseña ni validación.</p></li>
      <li><p>Todas las vistas deben mostrar en la parte superior el nombre del usuario o el texto
        "invitado". Junto a invitado debe aparece un texto que sea "login" o "logout" según corresponda
        con href /login/index o /login/out.</p></li>
      <li><p>/login/out Debe eliminar la sesión y volver a la página de inicio.</p></li>
      <li><p>/client/index/$page Debe mostrar la lista de todos los clientes. La lista debe
        ir paginada en bloques de 5 en 5. Si el parámetro index no se indica debe mostrar la página 5.</p></li>
      <li><p>/client/new Debe mostrar un formulario de alta. El action del formulario será /client/insert.</p></li>
      <li><p>/client/edit/$id Debe mostrar un formulario de edición de datos.
        El action del formulario será /client/update.</p></li>
      <li><p>/client/delete/$id Debe borrar un registro de cliente.</p></li>
      <li><p>/client/search Debe mostrar un campo de búsqueda de cliente por nombre.
        El action del formulario será /client/index Debes modificar el controlador correspondiente
        para que si se reciben peticiones de búsqueda la lista se ajuste a dicho parámetro.
        NOTA: si se busca por el texto "ara" deben aparecer todos los clientes cuyo nombre contenga
        ese fragmento.</p></li>
    </ul>
    </div>
    
    <div id="footer"> Pie de página</div>

</body>
</html>
