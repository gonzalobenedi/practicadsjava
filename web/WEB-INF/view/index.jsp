<%-- 
    Document   : index
    Created on : 11-dic-2016, 12:56:26
    Author     : gonza
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/default.css">
    <title>Práctica Java</title>
</head>
<body>
<div id="header">
<div id="title">Práctica Java 1ª Evaluación</div>
<div>
    <a href="<%= request.getContextPath() %>/client">Cliente</a>
    <a href="<%= request.getContextPath() %>/help">Ayuda</a>
</div>
</div>

    
    <div id="content">
        <% 
    try{
        if(!request.getSession().getAttribute("username").equals("")){
    %>
            <p>Hola <%=request.getSession().getAttribute("username")%> || <a href="<%= request.getContextPath() %>/login/out">Logout</a></p>
    <%
        }else{
    %>
            <p>Hola Invitado || <a href="<%= request.getContextPath() %>/login">Login</a></p>
    <%
        }
    }catch(Exception e){
    %>
    <p>Hola Invitado || <a href="<%= request.getContextPath() %>/login">Login</a></p>
    <%
    }
%>

        <h2>Login</h2>
        <form action="<%= request.getContextPath() %>/login/in" method="post">
            <p>Nombre: <input type="text" name="username"></p>
            <input type="submit" value="Entrar">
        </form>

    </div>
    
    <div id="footer"> Pie de página</div>

</body>
</html>
