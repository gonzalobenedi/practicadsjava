<%-- 
    Document   : clientEdit
    Created on : 11-dic-2016, 12:21:03
    Author     : gonza
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/default.css">
    <title>Práctica Java</title>
</head>
<body>
<div id="header">
<div id="title">Práctica Java 1ª Evaluación</div>
<div>
    <a href="<%= request.getContextPath() %>/client">Cliente</a>
    <a href="<%= request.getContextPath() %>/help">Ayuda</a>
</div>
</div>

    
    <div id="content">
        <% 
    try{
        if(!request.getSession().getAttribute("username").equals("")){
    %>
            <p>Hola <%=request.getSession().getAttribute("username")%> || <a href="<%= request.getContextPath() %>/login/out">Logout</a></p>
    <%
        }else{
    %>
            <p>Hola Invitado || <a href="<%= request.getContextPath() %>/login">Login</a></p>
    <%
        }
    }catch(Exception e){
    %>
    <p>Hola Invitado || <a href="<%= request.getContextPath() %>/login">Login</a></p>
    <%
    }
%>

        <h2>Edición de Cliente</h2>
        <form action="<%= request.getContextPath() %>/client/update" method="post">
            <jsp:useBean id="client" class="model.Client" scope="request"/>
            <p>Nombre: <input type="text" name="username" value="<%=client.getName()%>" ></p>
            <p>Dirección: <input type="text" name="address" value="<%=client.getAddress()%>" ></p>
            <p>Teléfono: <input type="text" name="phone" value="<%=client.getPhone()%>" ></p>
            <p>Credito: <input type="text" name="credit" value="<%=client.getCredit()%>" ></p>
            <input type="hidden" name="id" value="" value="<%=client.getId()%>" >
            <input type="submit" value="Actualizar">
        </form>

    </div>
    
    <div id="footer"> Pie de página</div>

</body>
</html>
