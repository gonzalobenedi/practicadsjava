<%-- 
    Document   : clientNew
    Created on : 11-dic-2016, 12:43:10
    Author     : gonza
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/default.css">
    <title>Práctica Java</title>
</head>
<body>
<div id="header">
<div id="title">Práctica Java 1ª Evaluación</div>
<div>
    <a href="<%= request.getContextPath() %>/client">Cliente</a>
    <a href="<%= request.getContextPath() %>/help">Ayuda</a>
</div>
</div>

    
    <div id="content">
        <% 
    try{
        if(!request.getSession().getAttribute("username").equals("")){
    %>
            <p>Hola <%=request.getSession().getAttribute("username")%> || <a href="<%= request.getContextPath() %>/login/out">Logout</a></p>
    <%
        }else{
    %>
            <p>Hola Invitado || <a href="<%= request.getContextPath() %>/login">Login</a></p>
    <%
        }
    }catch(Exception e){
    %>
    <p>Hola Invitado || <a href="<%= request.getContextPath() %>/login">Login</a></p>
    <%
    }
%>
        
        <h2>Añadir Cliente</h2>
        <form action="<%= request.getContextPath() %>/client/insert" method="post">
            <p><label> Nombre: </label><input type="text" name="username" value="" ></p>
            <p><label>Dirección: </label><input type="text" name="address" value="" ></p>
            <p><label>Teléfono: </label><input type="text" name="phone" value="" ></p>
            <p><label>Credito: </label><input type="text" name="credit" value="" ></p>
            <input type="submit" value="Añadir">
        </form>

    </div>
    
    <div id="footer"> Pie de página</div>

</body>
</html>
