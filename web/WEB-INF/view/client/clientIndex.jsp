<%-- 
    Document   : clientIndex
    Created on : 11-dic-2016, 12:20:49
    Author     : gonza
--%>

<%@page import="model.Client"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/default.css">
    <title>Práctica Java</title>
</head>
<body>
<div id="header">
<div id="title">Práctica Java 1ª Evaluación</div>
<div>
    <a href="<%= request.getContextPath() %>/client">Cliente</a>
    <a href="<%= request.getContextPath() %>/help">Ayuda</a>
</div>

</div>

    
    <div id="content">
        <% 
    try{
        if(!request.getSession().getAttribute("username").equals("")){
    %>
            <p>Hola <%=request.getSession().getAttribute("username")%> || <a href="<%= request.getContextPath() %>/login/out">Logout</a></p>
    <%
        }else{
    %>
            <p>Hola Invitado || <a href="<%= request.getContextPath() %>/login">Login</a></p>
    <%
        }
    }catch(Exception e){
    %>
    <p>Hola Invitado || <a href="<%= request.getContextPath() %>/login">Login</a></p>
    <%
    }
%>

        <p><a href="<%= request.getContextPath() %>/client/new">Añadir Cliente</a> | <a href="<%= request.getContextPath() %>/client/search">Buscar cliente</a></p>
        <h2>Listado de clientes</h2>
        <jsp:useBean id="users" class="ArrayList<model.Client>" scope="request"/>  
        <table>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Dirección</th>
                <th>Teléfono</th>
                <th>Credito</th>
            </tr>
            <%
                Iterator<model.Client> iterator = users.iterator();
                while(iterator.hasNext()) {
                    Client client = iterator.next();
                    %>
                    <tr>
                        <td><%= client.getId()%></td>
                        <td><%= client.getName()%></td>
                        <td><%= client.getAddress()%></td>
                        <td><%= client.getPhone()%></td>
                        <td><%= client.getCredit()%>€</td>
                    </tr>
                    <%
                }
            %>
        </table>
        
        

    </div>
    
    <div id="footer"> Pie de página</div>

</body>
</html>
